﻿using Ejemplo_Web_App.Models; //Importamos los modelos
using System;
using System.Collections.Generic;
using System.Data.Entity; //Importamos la librería EF
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Ejemplo_Web_App.DAL
{
    public class BookContext : DbContext //Es el que permite realizar consultas a la  BD
    {
        public BookContext():base("BookContext")
        {

        }

        public DbSet<Book> Books { get; set; }

        public DbSet<Author> Authors { get; set; }

        /*Sobre escribiendo la funcion por default OnModelCreating se pueden especificar
         diferentes opciones de como se crean las tablas dentro de la BD. En este caso 
         se le dice a EF que remieva la convencion de pluralizar el nombre de las tablas.*/
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}